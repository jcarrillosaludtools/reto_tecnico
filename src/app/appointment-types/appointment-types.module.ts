import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentTypesAdminComponent } from './appointment-types-admin/appointment-types-admin.component';



@NgModule({
  declarations: [
    AppointmentTypesAdminComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AppointmentTypesAdminComponent
  ]
})
export class AppointmentTypesModule { }
