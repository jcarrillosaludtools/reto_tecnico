export interface AppointmentType {
  id: number;
  name: string;
  description?: string;
  color: string;
  duration: number;
  isActived: boolean;
  create_date: Date;
  edition_date?: Date;
}
