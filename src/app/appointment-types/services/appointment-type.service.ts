import { Observable, of } from "rxjs";
import { AppointmentType } from "../model";

let info: AppointmentType[] = [
  {id: 1, name: 'Consulta Control', description: 'Consulta Control',
  color: '#FF00FF', duration: 30, isActived: true, create_date: new Date()},
  {id: 2, name: 'Cita de primera Vez', description: 'Cita de primera Vez',
  color: '#008000', duration: 45, isActived: true, create_date: new Date()},
  {id: 3, name: 'Consulta Cardiología',
  color: '#008080', duration: 15, isActived: false, create_date: new Date(), edition_date: new Date()},
  {id: 4, name: 'Consulta Pediatria', description: 'Consulta Pediatria',
  color: '#800080', duration: 30, isActived: false, create_date: new Date(), edition_date: new Date()},
  {id: 5, name: 'Seguimiento', description: 'Seguimiento',
  color: '#F7DC6F', duration: 60, isActived: true, create_date: new Date()}
];

export class AppointmentTypeService{

returnAll(): Observable<AppointmentType[]>{
  return of(info)

}

}
