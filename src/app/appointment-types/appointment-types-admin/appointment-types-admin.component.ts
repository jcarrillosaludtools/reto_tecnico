import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppointmentType } from '../model';
import { AppointmentTypeService } from '../services/appointment-type.service';

@Component({
  selector: 'app-appointment-types-admin',
  templateUrl: './appointment-types-admin.component.html',
  styleUrls: ['./appointment-types-admin.component.scss']
})
export class AppointmentTypesAdminComponent implements OnInit {
  appointmentTypeService: AppointmentTypeService;
  appointmentTypes$: Observable<AppointmentType[]> | undefined;

  constructor() {
    this.appointmentTypeService = new AppointmentTypeService();
  }

  ngOnInit(): void {
    this.appointmentTypes$ = this.appointmentTypeService.returnAll();
  }


}
