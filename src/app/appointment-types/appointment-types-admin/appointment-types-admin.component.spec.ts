import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentTypesAdminComponent } from './appointment-types-admin.component';

describe('AppointmentTypesAdminComponent', () => {
  let component: AppointmentTypesAdminComponent;
  let fixture: ComponentFixture<AppointmentTypesAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointmentTypesAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentTypesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
